#!/usr/bin/env python3

#inheritance from built-in class, str is a built-in python class
class RevStr(str):
    def __str__(self):
        return self[::-1]  # step size -1, represent backward i.e reverse

def main():
    hello = RevStr('Hello, World.')
    print(hello)

if __name__ == '__main__': main()
