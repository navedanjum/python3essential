#!/usr/bin/env python3

hungry1 = True
x = 'Feed the bear now!' if hungry1 else 'Do not feed the bear.'
print(x)


hungry2 = False
x = 'Feed the bear now!' if hungry2 else 'Do not feed the bear.'
print(x)

# None is false
hungry2 = None
x = 'Feed the bear now!' if hungry2 else 'Do not feed the bear.'
print(x)