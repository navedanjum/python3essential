#!/usr/bin/env python3

class myString(str):
    def __str__(self):
        return self[::-1]

print('Hello, World.'.islower())
print("Hello".lower())
print("Hello".upper())

s = myString("Navedanjum Ansari")
print(s)