#!/usr/bin/env python3

print('Hello World'.capitalize())
print('Hello World'.title())
print('Hello World'.swapcase())
print('Hello World'.casefold())  #casefold i aggressive as it will lower case even unicode like StraBe(German)
print('Hello World'.upper())
print('Hi' + ' ' + 'Hello')

x = 42
y = 30
print('number is {}'.format(x))
print('numbers are {}, {}'.format(x,y))
print('numbers are {0}, {1}'.format(x,y))
print('numbers are {1}, {0}'.format(x,y))
print('numbers are {0}, {1} , {1}, {0}, {1}'.format(x,y))
print('numbers are {0}, {1:>05}'.format(x,y))
print('numbers are {0:+}, {1:+}'.format(x,y))
print(f'number is {x}')

z = 345*1000*5
print('numbers are {:,}'.format(z))
print('numbers are {:,}'.format(z).replace(',','.'))

#specify fix number of decimal places
print('numbers are {:.3f}'.format(z))
print('numbers are {:.2f}'.format(z))

#formatting as different number system
z = 13
print('numbers are {:x}'.format(z))
print('numbers are {:b}'.format(z))
print('numbers are {:o}'.format(z))
print(f'number is {z:.3f}')
print(f'number is {z:b}')