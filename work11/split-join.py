#!/usr/bin/env python3

s = 'This is a long string with a bunch of words in it.'
print(s)

#split into a list - default separator is space
print(s.split())
print(s.split('t'))

#join the list using :
l = s.split()
s2 = ':'.join(l)
print(s2)
