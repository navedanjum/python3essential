#!/usr/bin/env python3

secret = 'swordfish'
pw = ''
maxCount= 5
count=0
auth = False
while pw != secret:
    count +=1
    if count > maxCount:break
    pw = input("What's the secret word? ".format(count))
else:
    auth = True

print("successfully logged in ...." if auth else "Call Police ......")