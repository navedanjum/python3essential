#!/usr/bin/env python3

animals = ( 'bear', 'bunny', 'dog', 'cat', 'velociraptor' )

for pet in animals:
    print(pet)

for i in range(5):
    print(i)

for i in range(10,45,5):
    print(i)

for i in range(100,110):
    print(i)

# continue, break and else are the additional controls with the loops

#Behaviour of continue and else
for pet in animals:
    if pet == 'dog': continue
    print(pet)
else:
    print('list of animals')

#Behaviour of break and else .. note else is not executed
for pet in animals:
    if pet == 'dog': break
    print(pet)
else:
    print('list of animals')




