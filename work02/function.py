#!/usr/bin/env python3

def function(n):
    print(n)
    return  n*10

def myfunction(n):
    print(n*2)


function(47)
function(1000)

x = function(23)
print(x)

# None represents absence of value
y = myfunction(100)
print(y)