#!/usr/bin/env python3

x = 12
y = 12

if x < y:
    print('x < y: x is {} and y is {}'.format(x, y))
elif x > y:
    print ('x > y: x is {} and y is {}'.format (x, y))
elif x == y:
    print ('x == y: x is {} and y is {}'.format (x, y))
else:
    print('Do something')


# alternate way of using block on the single line
if x < y: print('x < y: s is {} and y is {}'.format(x,y))