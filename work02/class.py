#!/usr/bin/env python3


class Duck:
    def quack(self):
        print('Quaaack!')

    def walk(self):
        print('Walks like a duck.')

class Horse:
    def eat(self):
        print('Eats the grass')

    def type(self):
        print('It \'s a racing horse')

def main():
    donald = Duck()
    donald.quack()
    donald.walk()
    mojo = Horse()
    mojo.eat()
    mojo.type()


if __name__ == '__main__': main()
