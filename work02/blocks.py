#!/usr/bin/env python3

x = 42
y = 73

if x < y:
    z = 230
    print('x < y: x is {} and y is {}'.format(x, y))
    print('This is under the same block-if')

print('The value of z is not limited inside block and z is {}'.format(z))
#blocks do not define scope of the variable

