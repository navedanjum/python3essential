#! /usr/bin/env python3

import sqlite3

db = sqlite3.connect ('db-api.db')
cur = db.cursor ()

def main():
    cur.execute("SELECT COUNT(*) FROM test")
    count = cur.fetchone()[0]
    print(f'there are {count} rows in the table.')

    for row in cur.execute ("SELECT * FROM test"):
        print (row)

    clean_up()

def clean_up():
    print ('drop')
    cur.execute ("DROP TABLE test")
    print ('close connection')
    db.close()

if __name__ == '__main__' : main()