#!/usr/bin/env python3

def main():
    infile = open('berlin.jpg', 'rb')
    outfile = open('berlin-copy.jpg', 'wb')   #mode is write(w) and binary(b)
    while True:
        buf = infile.read(10240)  #Choose buffer size to write each time
        if buf:
            outfile.write(buf)
            print('.', end='', flush=True)
        else: break
    outfile.close()
    print('\ndone.')

if __name__ == '__main__': main()
