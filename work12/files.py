#!/usr/bin/env python3

def main():
    f = open('lines.txt','r')
    for line in f:
        print(line.rstrip())  #strips white space from the end of the line

if __name__ == '__main__': main()
