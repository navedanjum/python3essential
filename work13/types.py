#!/usr/bin/env python3

x = '-47'
y = int(x)
a = abs(y)

print(f'x is {type(x)}')
print(f'x is {x}')
print(f'y is {type(y)}')
print(f'y is {y}')
print(f'a is {a}')

z =float(x)
print(f'y is {type(z)}')

x = 47
print(divmod(x,3))  #returns a tuple of (quotient, remainder)

y = x + 33j
print(f'y is {y}')
print(f'y is {type(y)}')

#create complex number using complex constructor
y = complex(x,53)
print(f'y is {y}')
print(f'y is {type(y)}')