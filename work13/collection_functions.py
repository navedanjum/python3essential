#!/usr/bin/env python3

x = (1,2,3,4,5)
y = min(x)

print(x)
print(id(x))
print(y)

y = max(x)
print(y)

y = all(x)
print(y)

x = (0,0,0,0)
y = any(x)
print(y)

y = enumerate(x)
print(y)

for i,v in y: print(f'{i}: {v}')

x = (1,2,3,4)
y = (100,101,102,103)

z = zip(x,y)
print(z)

for a,b in z: print(a,b)