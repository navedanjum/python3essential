#!/usr/bin/env python3

class myclass:
    def __init__(self,n):
        self._n = n

    def __repr__(self):
        return f' repr:  number is {self._n}'
        return f' repr:  number is {self._n}'

    def __str__(self):
        return f' str: number is {self._n}'


s = myclass(100)
print(repr(s))   #repr prefers the __repr__ function
print(s)         #print prefers the __str__ function and then __repr__ function

print(ascii(s))  #same as repr but skips any special character

print(chr(128406))  # unicode representation of hand emoji -128406
print(ord('🖖'))       # print unicode representation of special character emoji

