#!/usr/bin/env python3

import sys
import os
import datetime

def main():
    v = sys.version_info
    print('Python version {}.{}.{}'.format(*v))

    z = sys.api_version
    print(z)
    print(list(sys.builtin_module_names))

    print(os.name)
    print(datetime.datetime.now())
    print(datetime.datetime.now().year)
    print (datetime.datetime.now ().day)

if __name__ == '__main__': main()