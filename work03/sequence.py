#!/usr/bin/env python3

#List - mutable
x = [ 1, 2, 3, 4, 5 ]
x[4] = 100 # List is a mutable sequence

for i in x:
    print('i is {}'.format(i))
    print(f'{i}')

#Tupe 0 immutable
y = ( 1, 2, 3, 4, 5 )
#  y[4] = 100   # tuple object does not support item assignment
for i in y:
    print(f'{i}')


z = range(10,100,5)   # range object is also immutable
for i in z:
    print(i)

z = list(range(10,100,5)) # make it a list to make it mutable
z[1] = 500
for i in z:
    print(f'vale of z is {i}')


# Dictionary objects

dict1 = {'x': 1, "y":2, "z":3}
for k,v in dict1.items():
    print(f'Key --> {k},  Value --> {v}')




