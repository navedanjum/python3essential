#!/usr/bin/env python3

x = (1, "Hello", [1,2], ('three','four'))
y = (1, "Hello", [1,2], ('three','four'))
z = [1, "Hello", [1,2], ('three','four')]

#Both x,y have the same type i.e tuple
print(type(x))
print(type(y))
print(type(z))

#All three x,y,z have different ids
print(id(x))
print(id(y))
print(id(z))

#Note the elements in different sequence types have same ids
print('Printing ids ................')
print(id(x[1]))
print(id(y[1]))
print(id(z[1]))


if isinstance(x, list):
    print('list')
elif isinstance(x, tuple):
    print('tuple')
else:
    print('Neither')

