#!/usr/bin/env python3

x = 7
print('x is {}'.format(x))
print(type(x))

y = None

b = f'value of x is {x}' # f string similar to format class
z = 'Hello'.capitalize()
w = "My world ".lower()
d = ''' 
     My world is here --> Style choice 
  '''
print(type(2/3))
print(type(3.7))
print(type(4))
print(type('Hi'))
print(type(y))   #NoneType

print(d)
print(z)
print(w)
print(b)

## Problem with the floating point calculation
i = 0.1 + 0.1 + 0.1 - 0.3

print(i) #Note the answer is not zero there use Decimal module for correct calculation
from decimal import *    # syntax to import everything from the decimal library

l = Decimal('0.1')
m = Decimal('0.3')

o = l + l + l - m
print(o)  #Note the answer is correct i.e zero (0.0)


flag1 = True
flag2 = False
result = 5 > 2

print(type(flag1))
print(type(flag2))
print(type(result))


value = "" #Empty string and None  evaluates to false
value = None  #None evaluates to false
value = 0  # 0 evaluates to false

value = 100 # Anything else evaluates to true

if value:
    print('True')
else:
    print('False')


