#!/usr/bin/env python3

import sys

def main():
    try:
        #x = 5/0
        y = int('foo')
    # except ValueError:
    #     print('Value error caught')
    # except ZeroDivisionError:
    #     print('Don\'t divide by zero')
    except:
        print(f'unknown error:{sys.exc_info()[1]}')
    else:
        #print(x)
        print(y)
        print('good job')

if __name__ == '__main__': main()