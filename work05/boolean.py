#!/usr/bin/env python3


a = True
b = False
x = ( 'bear', 'bunny', 'tree', 'sky', 'rain' )
y = 'bear'

if a and b:
    print('expression is true')
else:
    print('expression is false')


if y in x:
    print('true')
else:
    print('false')


if y is x[0]:
    print('true')
else:
    print('false')

if y is not x[1]:
    print('true')
else:
    print('false')

#Note object id is same
print(id(x[0]))
print(id(y))