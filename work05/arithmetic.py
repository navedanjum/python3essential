#!/usr/bin/env python3


x = 'Mo'
print(x*3*2)


x = 5
y = 3
z = x + y

print(f'result is {z * 2}')
print(f'result is {z}')

z = x - y
print(f'result is {z}')

print(f'result is {x/y}')

#Note you need // to get the answer as integer.
print(f'result is {x//y}')