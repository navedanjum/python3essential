# Python 3 Essential Training Scripts

## Description
* Last time, I used Python 2 for writing SQL like test scripts on Ubuntu platform in 2012-2013
* This training scripts are created to get on track once again to start using python 3
* Python scripts are written using jet brains PyCharm IDE in Python's latest version 3.7.2
* Scripts are basics and all basic python 3 essentials are revisited to get started coding advance programs in Python

## About author
* Navedanjum Ansari
* Linkedin: https://www.linkedin.com/in/navedanjum-a-6018783b/
* As an SDET, it has always been easy to pick up new language concepts and syntax to deliver a test solution
* At the same time, it becomes difficult to keep hold of different language syntax when not used for long time.
* This repository is created to quickly go through the syntax and revisit, if required to code again in Python.


