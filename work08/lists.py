#!/usr/bin/env python3

#List is mutable
def main():
    game = [ 'Rock', 'Paper', 'Scissors', 'Lizard', 'Spock' ]
    print_list(game)
    game[1] = 'Soft Rock'
    print_list(game)
    game.append('Jack')
    print_list(game[1:len(game)-1])
    print(game.index('Lizard'))
    game.insert(1,'Test')
    print_list(game)
    game.remove('Soft Rock')
    print_list(game)
    game.pop()
    print_list(game)
    removed_item = game.pop(1)
    print_list(game)
    print(removed_item)

    del game[0]
    print_list(game)

# deletes 2 items starting from index 0 i.e item at index 0 and item at index 1
    del game[0:2]
    print_list(game)

# will delete every second element starting from 1 till index 4
    game = ['Rock', 'Paper', 'Scissors', 'Lizard', 'Spock']
    del game[1:4:2]
    print_list(game)
    print(','.join(game))

# Tuple is immutable and so you cannot modify/add/append but rest of the operations remain same.
    game = ('Rock', 'Paper', 'Scissors', 'Lizard', 'Spock')
    print_list(game)

def print_list(o):
    for i in o: print(i, end=' ', flush=True)
    print()

if __name__ == '__main__': main()