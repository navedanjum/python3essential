#!/usr/bin/env python3

# set is unordered, i.e each time you get output in any random order
# set is like a list but it does not allow duplicate elements

def main():
    a = set("We're gonna need a bigger boat.")
    b = set("I'm sorry, Dave. I'm afraid I can't do that.")
    print_set(a)
    print_set(b)
    print_set(a - b)
    print_set(a | b)
    print_set(a & b)
    print_set(a ^ b)
    print(a)

def print_set(o):
    print('{', end = '')
    for x in o: print(x, end = '')
    print('}')

if __name__ == '__main__': main()
