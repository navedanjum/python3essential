#!/usr/bin/env python3

from math import pi
#List comprehension
def main():
    seq = range(11)
    seq1= [x*x for x in seq]
    seq2= [x for x in seq if x%3 !=0]
    seq3= [round(pi,i) for i in seq]

    #creating a dictionary using comprehension
    seq4= {x:x**3 for x in seq}

    #creating a dictionary using comprehension
    seq5= {x for x in "superdreams" if x not in "aeiou"}

    print_list(seq)
    print_list (seq1)
    print_list (seq2)
    print_list (seq3)
    print(seq4)
    print_list(seq5)

def print_list(o):
    for x in o: print(x, end = ' ')
    print()

if __name__ == '__main__': main()
