#!/usr/bin/env python3

def main():
    animals = { 'kitten': 'meow', 'puppy': 'ruff!', 'lion': 'grrr',
        'giraffe': 'I am a giraffe!', 'dragon': 'rawr' }
    print_dict(animals)
    print_keys(animals)
    print(animals['kitten'])
    print(animals.get('puppy'))
    print('found' if 'dragon1' in animals.keys() else 'not found')
    print ('found' if 'dragon' in animals else 'not found')

def print_dict(o):
    #for x in o: print(f'{x}: {o[x]}')
    for k,v in o.items(): print(f'{k}:{v} ')

def print_keys(o):
    for k in o.keys(): print(f'{k}')
    for v in o.values(): print(f'{v}')

if __name__ == '__main__': main()